# smbjr

Extremely barebones (but compileable) Super Monkey Ball Jr. disassembly.

It builds the following ROM:
* [smbjr.gba](https://datomatic.no-intro.org/index.php?page=show_record&s=23&n=0747) `sha1: 0d994a58b7acdfe9357e5405acebe232128b80fe`


# Building
To build the project you need to have devkitarm installed.

1. Copy a USA ROM of Super Monkey Ball Jr. into this directory and name it `baserom.gba`

2. Run `make`
