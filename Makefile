ifeq ($(strip $(DEVKITARM)),)
$(error "Please set DEVKITARM in your environment. export DEVKITARM=<path to>devkitARM)
endif

AS = $(DEVKITARM)/bin/arm-none-eabi-as
OBJCOPY = $(DEVKITARM)/bin/arm-none-eabi-objcopy
LD = $(DEVKITARM)/bin/arm-none-eabi-ld

ASFLAGS = -mthumb-interwork -march=armv4t

.PHONY: clean all compare

all: smbjr.gba

clean:
	rm smbjr.o smbjr.elf smbjr.gba

compare: smbjr.gba
	sha1sum -c smbjr.sha1

smbjr.o : asm/smbjr.s
	@echo asm: $< -> $@
	$(AS) $(ASFLAGS) $< -o $@

smbjr.elf: smbjr.o ld.txt
	$(LD) -o smbjr.elf -T ld.txt

smbjr.gba : smbjr.elf
	$(OBJCOPY) -O binary $< $@


